## Add a new package

The process here is clunky because we don't have a proper cargo registry that allows `publish` operations but works fine for low traffic registries

This is the process

- check out the forge-cargo repo from Bitbucket
- create a repo inBitbucket with your exact package name - including case
- write your crate
- copy over package.py from forge-cargo to your repo
- run package.py
- it will ask you to check in your release/ folder alongwith anything else you need to check in from your crate;
- note that the version you have on the Cargo.toml is the version of the crate published. If you are bumping up crate versions, make sure this is handled
- it will also ask you to either check in a newly created directory for the crate index or the updated index 

## Using the custom registry

This needs to be done on your local development setup

- edit ~/.cargo/config
- add the following

[registries.forge]

index = "https://bitbucket.org/smithsdigitalforge/forge-cargo.git"
- save the file 

At this point, you should be able to add these private crates to your Cargo.toml and use them like any crate from crates.io

## If you are contantly updating crates during development

- couple of steps you need to handle
- clean the Cargo.lock every time you update the crate in Bitbucket. It preserves the hash
- if you see hash errors, manually delete the cached crate from ~/.cargo/registry/src/bitbucket.org..../ (or delete the entire bitbucket.org registry and re-fetch your packages using `cargo fetch`)

## Using these crates

the one additional parameter in this case is the registry the crate is located at.
Something like

[dependencies]

hdlpack = { version = "0.1.0", registry = "forgereg" }

will fetch the 0.1.0 version of th hdlpack crate

## Rust compatibility

Rust environment seems to be evolving all the time. You need a Rust version > 1.35. If you have rustup installed, it maybe a good idea to update your rust in case your rust installation is outdated. 
