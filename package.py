import os, sys
import shutil
from os import listdir
from os.path import isfile, join
import toml
import json
import hashlib
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient, __version__

def crateversioncorrect(workingdir, cratename, crateversion):
  print('Checking crate: ' + cratename + ' version: ' + crateversion)
  parentdir = cratename[0:2]
  childdir = cratename[2:4]
  filepath = workingdir + '/' + parentdir + '/' + childdir + '/' + cratename;
  versioncorrect = True
  print('Checking file: ' + filepath)
  if os.path.exists(filepath):
    print('file exists.')
    for line in open(filepath,'r'):
            entry = ''
            try:
                print('reading: ',line)
                entry = json.loads(line)
                print(entry['vers'] + ' vs ' + crateversion)
                if entry['vers'] == crateversion:
                    versioncorrect = False 
                    break
            except Exception as e:
                print('json error:', e, line)
  return versioncorrect

if len(sys.argv) < 2:
    print('need the crate path as an arg')
    sys.exit(0)

cargopath = sys.argv[1]
pwd = os.getcwd()
os.chdir(cargopath)
cargofile = toml.loads(open('Cargo.toml','r').read())
cargoname = cargofile['package']['name']
cargovers = cargofile['package']['version']
## now checks to see if this version of the crate to publish already exists in the registry
if not crateversioncorrect(pwd, cargoname, cargovers):
  print('Error: check for Crate version correctness, ' + cargoname + ' ' + cargovers)
  exit()

local_path = cargopath + "/target/package/"
crate_file_name = cargoname + "-" + cargovers + ".crate"
local_file_path = os.path.join(local_path, crate_file_name)
if os.path.exists(local_file_path):
  os.remove(local_file_path)

os.system('cargo package --allow-dirty')
try:
    os.mkdir('release')
except FileExistsError as e:
    pass
mypath = './target/package'
hashval = hashlib.sha256()
for f in os.listdir(mypath):
    if os.path.isfile(join(mypath, f)) and '.crate' in join(mypath,f):
        shutil.copyfile(join(mypath,f),'release/'+f)
        hashval.update(open(join(mypath,f),'rb').read())
        print(hashval.hexdigest())
        break; ## break on the first cargo package

os.chdir(pwd)
## TODO: handle deps from the Cargo.toml 

deps = cargofile['dependencies']

newdeps = []

for key in deps:
   value = deps[key]
   if isinstance(value,dict):
       req = value['version']
       features = value['features']
   else:
       req = value
       features = []

   newentry = {"name": key, "req": req, "features": features, "optional": False, "default_features": True, "target": None, "kind": "normal", "registry": "https://github.com/rust-lang/crates.io-index", "package": None}

#   newentry = {"name": key, "req": req, "features": features}

   print('newentry: ', newentry)

   newdeps.append(newentry)

package={
        'name': cargofile['package']['name'],
        'vers': cargofile['package']['version'],
        'deps': newdeps,
        'cksum': hashval.hexdigest(),
        'features': {},
        'yanked': False,
        'links': None
        }
print(json.dumps(package))
##TODO - assuming at least 4 chars in our package name for now
parentdir=cargofile['package']['name'][0:2]
childdir=cargofile['package']['name'][2:4]
try:
    try:
        os.mkdir(parentdir)
    except FileExistsError as e:
        pass
    try:
        os.mkdir(parentdir+'/'+childdir)
    except FileExistsError as e:
        pass

#    file = open(parentdir+'/'+childdir+'/'+cargofile['package']['name'],'a')
#    file.write('"name":"%s","vers":"%s","deps":[' % (cargofile['package']['name'], str(cargofile['package']['version'])))
#    for dep in newdeps:
#      file.write('{"name":"%s","req":"%s","features":%s,""optional":false,"default_features":true,"target":null,"kind":"normal","registry":"https://github.com/rust-lang/crates.io-index","package":null}' % (dep['name'], dep['req'], dep['features']))

#    file.write('\n')
#    file.close()

    try:
      open(parentdir+'/'+childdir+'/'+cargofile['package']['name'],'a').write(json.dumps(package)+'\n')

    except Exception as err:
        print('error:',err)

    try:
      connect_str = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
      blob_service_client = BlobServiceClient.from_connection_string(connect_str)
      container_name = "$web"
      container_client = blob_service_client.get_container_client(container_name)
      print("\nFile to upload:\n\t" + local_file_path)

      upload_prefix = "releases/cargo"
      upload_file_path = os.path.join(upload_prefix, crate_file_name)

      # Create a blob client using the local file name as the name for the blob
      blob_client = blob_service_client.get_blob_client(container=container_name, blob=upload_file_path)

      print("\nUploading to Azure Storage as blob:\n\t" + upload_file_path)

      # Upload the created file
      with open(local_file_path, "rb") as data:
        blob_client.upload_blob(data)


    except Exception as ex:
      print('Exception:', ex)


    print('Please check-in the %s/%s/%s file in the forge-cargo repository'%(parentdir, childdir,cargofile['package']['name']))

except Exception as e:
    print('error:',e)


